package slack

import play.api.{Configuration, Logger}
import v1.post.PostData
import javax.inject.{Inject, Singleton}
import play.api.libs.ws.WSClient
import play.api.libs.json._

@Singleton
class SlackService @Inject()(ws: WSClient, config: Configuration) {

  private val logger = Logger(this.getClass)

  private val url: String = config.get[String]("slack.webhook-url")

  def sendMessage(message: PostData): Unit = {
    logger.trace(message.toString)
    val data = Json.obj("text" -> message.body)
    ws.url(url).post(data)
  }

}
