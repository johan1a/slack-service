FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim

WORKDIR /app

COPY target/universal/slack-service-*.zip slack-service.zip

RUN set -x \
  && apk add bash \
  && unzip -d /app slack-service.zip \
  && mv /app/*/* /app/ \
  && rm /app/bin/*.bat

EXPOSE 8080
CMD /app/bin/slack-service -Dhttp.port=8080
